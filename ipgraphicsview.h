#ifndef IPGRAPHICSVIEW_H
#define IPGRAPHICSVIEW_H

#include <QGraphicsView>

class IPGraphicsView : public QGraphicsView
{
public:
    IPGraphicsView();
    ~IPGraphicsView();

private:
    QGraphicsScene *scene;
    QImage *mainImage;
    void resizeEvent(QResizeEvent* event);

public slots:
    void openImageFile(const QString &file);
};

#endif // IPGRAPHICSVIEW_H
