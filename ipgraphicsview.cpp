#include <QMessageBox>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include "ipgraphicsview.h"

IPGraphicsView::IPGraphicsView()
{
    this->scene = new QGraphicsScene();
    this->setScene(this->scene);
    this->mainImage = nullptr;
}

IPGraphicsView::~IPGraphicsView()
{
    if(!this->mainImage)
        delete this->mainImage;
    delete scene;
}

void IPGraphicsView::resizeEvent(QResizeEvent *event)
{
    this->fitInView(this->scene->itemsBoundingRect(),
                    Qt::KeepAspectRatio);
}

void IPGraphicsView::openImageFile(const QString &file)
{
    if(!this->mainImage)
        delete this->mainImage;

    this->mainImage = new QImage(file);

    QGraphicsPixmapItem *mainPixmapItem =
        this->scene->addPixmap(QPixmap::fromImage(*this->mainImage));

    this->fitInView(this->scene->itemsBoundingRect(),
                    Qt::KeepAspectRatio);

    QMessageBox alert;
    alert.setText("File " +
                  file +
                  " info: \nresolution: "+
                  QString::number(this->mainImage->width())+
                  "x"+
                  QString::number(this->mainImage->height())+
                  "\ndepth: "+
                  QString::number(this->mainImage->depth())
    );
    alert.exec();
}
